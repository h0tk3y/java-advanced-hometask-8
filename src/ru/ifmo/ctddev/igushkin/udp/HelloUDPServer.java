package ru.ifmo.ctddev.igushkin.udp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.concurrent.*;

/**
 * Created by Sergey.
 */
public class HelloUDPServer {
    public static void main(String[] args) {
        try {
            new HelloUDPServer(Integer.parseInt(args[0])).start();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    private int port;

    public HelloUDPServer(int port) throws SocketException {
        this.port = port;
    }

    private boolean isStopping;

    private synchronized void stop() {
        isStopping = true;
    }

    private static final int MAX_THREADS = 10;

    private void start() throws SocketException {
        synchronized (this) {
            if (isStopping) return;
        }
        try (final DatagramSocket localEndpoint = new DatagramSocket(port)) {
            ExecutorService threads = Executors.newFixedThreadPool(MAX_THREADS);
            while (!isStopping)
                try {
                    final DatagramPacket p = new DatagramPacket(new byte[1024], 1024);
                    localEndpoint.receive(p);
                    threads.execute(new Runnable() {
                        @Override
                        public void run() {
                            String response = "Hello, " + Thread.currentThread() + " " + new String(p.getData(), 0, p.getLength());
                            byte[] responseBytes = response.getBytes();
                            try {
                                synchronized (localEndpoint) {
                                    localEndpoint.send(new DatagramPacket(responseBytes, responseBytes.length, p.getSocketAddress()));
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}
