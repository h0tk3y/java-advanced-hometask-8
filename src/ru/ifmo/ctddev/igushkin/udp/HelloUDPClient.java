package ru.ifmo.ctddev.igushkin.udp;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Sergey.
 */
public class HelloUDPClient {

    public static void main(String[] args) {
        new HelloUDPClient(args[0], Integer.parseInt(args[1]), args[2]).start();
    }

    public final String remoteAddress;
    public final int    remotePort;
    public final String messagePrefix;

    public HelloUDPClient(String remoteAddress, int remotePort, String messagePrefix) {
        this.remoteAddress = remoteAddress;
        this.remotePort = remotePort;
        this.messagePrefix = messagePrefix;
    }

    private boolean isStopping;

    private synchronized void stop() {
        isStopping = true;
    }

    private final int MAX_THREADS = 10;

    ExecutorService threads = Executors.newFixedThreadPool(MAX_THREADS);

    private void start() {
        for (int i = 0; i < MAX_THREADS && !isStopping; ++i)
            threads.execute(new HelloClientTask(i + 1));
    }

    private final int RESPONSE_TIMEOUT = 2000;

    private class HelloClientTask implements Runnable {

        final int number;

        private HelloClientTask(int number) {this.number = number;}

        @Override
        public void run() {
            try (DatagramSocket socket = new DatagramSocket()) {
                socket.setSoTimeout(RESPONSE_TIMEOUT);
                int requestsCount = 0;
                SocketAddress remoteEndpoint = new InetSocketAddress(remoteAddress, remotePort);
                while (!isStopping) {
                    String req = String.format("%s_%d_%d", messagePrefix, number, ++requestsCount);
                    byte[] reqBytes = req.getBytes();
                    DatagramPacket p = new DatagramPacket(reqBytes, 0, reqBytes.length, remoteEndpoint);
                    System.out.format("Client thread %d is sending \"%s\" to server.\n", number, req);
                    socket.send(p);
                    DatagramPacket response = new DatagramPacket(new byte[1024], 1024);
                    try {
                        socket.receive(response);
                        System.out.println(String.format("Client thread %d received \"%s\" from server.", number,
                                                         new String(response.getData(), 0, response.getLength())));
                    } catch (SocketTimeoutException e) {
                        System.out.println(String.format("Client thread %d timed out waiting for response.", number));
                    }
                }
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
